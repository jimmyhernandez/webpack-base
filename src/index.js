import _ from 'lodash';
function component() {
  let element = document.createElement('div');
  element.innerHTML = _.join(['Hello','webpack'],' ');
  return element;
}

document.body.appendChild(component());

fetch("http://api.open-notify.org/astros.json")
  .then(res => res.json())
  .then(console.log);

// Async and await
const githubRequest = async (loginName) => {
  try {
    var response = await fetch(`https://api.github.com/users/${loginName}/followers`);
    var json = await response.json();
    var followerList = json.map(user => user.login);
    console.log(followerList);
  } catch (e) {
    console.log("Error " + e);
  }
};

githubRequest("eveporcello");

// Classes
class Vehicle {
  constructor(description, wheels) {
    this.description = description;
    this.wheels = wheels;
  }

  describeYourself() {
    console.log(`Description ${this.description} - Wheels ${this.wheels}`);
  }
}

var coolSkiVan = new Vehicle('Cool Ski Van',4);

coolSkiVan.describeYourself();

class SemiTruck extends Vehicle {
  constructor() {
    super("Semi Truck",18);
  }
}

var groceryTruck = new SemiTruck();
groceryTruck.describeYourself();

// Getters and Setters
var attendance = {
  _list: [],
  set addName(name) {
    this._list.push(name);
  },
  get list() {
    return this._list.join(', ');
  }
};

attendance.addName = 'test';
attendance.addName = 'test2';
attendance.addName = 'test3';
console.log(attendance.list);
console.log(attendance._list);